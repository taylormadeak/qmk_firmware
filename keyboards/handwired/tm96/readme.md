# TM96

Hand wired keyboard with 99 Keys and a 3D printed case.

- Designed by Bob Taylor
- ANSI only
- Uses Arduino Pro Micro (or equivalent) with demultiplexers

Keyboard Maintainer: Bob Taylor

Hardware Supported: Pro Micro with 74LS138 and 74LS139 demultiplexer ICs

Hardware Availability: DIY

Make example for this keyboard (after setting up your build environment):

    make tm96:default

Install examples:

    make tm96:default:dfu

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
